## Reinforcement learning solves maze problems
# Core code
- `maze.py`: implementation of the maze class. The maze information is represented as a two-dimensional array, each number in the array represents a square, and the number value represents the type of square (e.g. 0 for wall, 2 for trap, 3 for fire).
- `train_qtable.py`: Implementation of the Q table class, which has functions for storing Q values, making decisions, learning Q tables, etc. It interacts with the maze ("environment") when making predictions and learning, inputting actions to it and getting feedback.
- `train_network.py`: implementation of supervised learning models for learning, prediction, etc.
- `git.py`: A batch learning repository for supervised learning.
- `maze_map.py`: stores 6 pre-defined mazes
- `draw.py`: visualization of the Q-table and rendering of the complete maze walking process.

## How to run
- Run `code/train_qtable.py` to train the maze defined in maze_map, and display the Q-table and the complete maze walking process after the training.

## Contributors
Zixuan Wang : zixuan.wang@eleve.ensai.fr